# HA-Proxy metric monitoring 

HA Proxy metrics monitoring using a basic golang based [exporter](https://gitlab.cern.ch/cloud-infrastructure/haproxy-metrics/-/blob/master/main.go). 

## Introduction

This chart manages the deployment of haproxy on a [Kubernetes](http://kubernetes.io)
cluster using the [Helm](https://helm.sh) package manager.

## Prerequisites

- Kubernetes cluster, version >=1.15.3
- Helm version >= 3.x
- [CRD-Servicemonitors](https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/crds/crd-servicemonitors.yaml) >= 0.56.2
```kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.56.2/example/prometheus-operator-crd/monitoring.coreos.com_servicemonitors.yaml```

## Installing the Chart

The file lbs.yaml should contain the loadbalancers that you would like to monitor. This file is created by [dblogger](https://gitlab.cern.ch/cloud-infrastructure/openstack-db-logger/-/blob/client/dblogger/producer/loadbalancer.py#L42). 

To install the chart with release name `my-release`:
```bash
helm install my-release haproxy-metrics --values https://s3.cern.ch/lb-haproxy/sdn1.yaml
```

## Uninstalling the Chart

To uninstall / delete deployment `my-release`:
```bash
helm delete my-release
```
