package main

import "os"
import "fmt"
import "net"
import "bufio"
import "log"
import "net/http"
import "bytes"

var buffer bytes.Buffer

func getMetrics(host string) []byte {
        conn, err := net.Dial("tcp", host + ":9999")
        if err != nil {
        	log.Println(err)
        }
        fmt.Fprintf(conn, "show stat\n")
	buffer.Reset()
	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		buffer.Write(scanner.Bytes())
		buffer.WriteString("\n")
	}
	return buffer.Bytes()
}

func main(){
        http.HandleFunc("/metrics", func(w http.ResponseWriter, r *http.Request) {
        	w.Write(getMetrics(os.Getenv("HAPROXY_HOST")))
        })
        
        log.Fatal(http.ListenAndServe("127.0.0.1:9999", nil))
}
