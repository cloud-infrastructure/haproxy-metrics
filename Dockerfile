FROM golang AS builder
WORKDIR /
ADD main.go /
RUN GO111MODULE=off CGO_ENABLED=0 GOOS=linux go build -o haproxy-metrics .
RUN chmod +x haproxy-metrics


CMD ["/haproxy-metrics"]

FROM alpine:latest
WORKDIR /
COPY --from=builder /haproxy-metrics /usr/local/bin
CMD ["/usr/local/bin/haproxy-metrics"]

